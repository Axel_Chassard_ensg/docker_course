# Docker_course

Ceci est dossier de rendus de mes TP de Docker.

Vous pouvez trouver dans le dossier "TP_Docker_Engine" les tp liés à la partie Docker Engine et da,s le dossier "TP_Docker_Compose" les tp liés à la partir Docker Compose.

Chaque tp possède un dossier portant le numéro du tp et comprenant des fichiers .md expliquant mon travail, ainsi que des dossiers de volume, des fichiers docker-file ou des fichiers docker-compose.yml. 

Bonne lecture.

# Auteur

Axel CHASSARD - Etudiant à l'ENSG-Géomatique, 3ème anné de cycle ingénieur, spécialité TSI - contact : axel.chassard@ensg.eu
