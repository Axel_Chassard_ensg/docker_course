# Etape 1

- commande : docker run -p 8080:80 -d --rm --name container_nextcloud nextcloud

- J'ai modifié la commande pour ajouter un volume : docker run -p 8080:80 -d --rm --name container_nextcloud -v /home/formation/docker_course/v_hote:/var/www/html nextcloud

# Etape 2

- On instancie un container mariadb : docker run --name database -e MYSQL_ROOT_PASSWORD=pswd -e MYSQL_DATABASE=mymap -e MYSQL_USER=user -e MYSQL_PASSWORD=s3cr3t -d --net netmap -v /home/formation/docker_course/tp2/v_hote_db:/var/lib/mysql mariadb:latest

- On instancie un conatiner nextcloud  : docker run -p 8080:80 -d --rm --name container_nextcloud -v /home/formation/docker_course/v_hote:/var/www/html --network=netmap nextcloud