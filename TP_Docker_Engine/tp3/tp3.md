# Un DockerFile pour GeoServer

J'ai commencé par faire le fichier dockerfile présent dans le dossier tp3.

Puis j'ai créé mon image avec la commande : docker image build /home/formation/docker_course/tp3/ --tag tomcat-geoserver

Et enfin j'ai créé un container pour cette image : docker run -d --rm --name containerGeoserver -p 8080:8080 tomcat-geoserver