<?php 
$OC_Version = array(20,0,0,9);
$OC_VersionString = '20.0.0';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '19.0' => true,
    '20.0' => true,
  ),
  'owncloud' => 
  array (
  ),
);
$OC_Build = '2020-10-02T16:51:32+00:00 615b994816710a595243d704c9be445f736b4b41';
$vendor = 'nextcloud';
