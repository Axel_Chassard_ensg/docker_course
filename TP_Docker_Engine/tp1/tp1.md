# Apache

## 1

commande : 

    docker container run -p 8080:80 -d httpd

La page http:127.0.0.1:8080 affiche "It works !"

## 2

commande : docker container run -v /home/formation/docker_course/tp1/v_hote:/usr/local/apache2/htdocs/ -p 8080:80 -d --rm httpd

## 3

La page affiché est désormais celle correspond au code html que nous avons recopié.

# Un peu de php

## 1

## 2

Cela ne fonctionne pas car pour l'image hhtpd, php n'est pas installé.

## 3

commande : docker container run -v /home/formation/docker_course/tp1/v_hote:/var/www/html/ -p 8080:80 -d --rm --name containerPhp lavoweb/php-7.1

puis j'ai changé le nom du fichier index.html en index.php.

# Avec une base de données

## 1

## 2

On commence par créé un réseau : docker network create netmap.
Puis on instancie un container de l'image mariadb, en respectant les informations de la ligne 3 du script php : docker run --name database -e MYSQL_ROOT_PASSWORD=pswd -e MYSQL_DATABASE=mymap -e MYSQL_USER=user -e MYSQL_PASSWORD=s3cr3t -d --net netmap mariadb:latest
Enfin, on connecte le container php au réseau que l'on a créé : docker network connect netmap awesome_wescoff.

## 3

On lance un deuxième processus sur le container lancé précédemment afin d'ouvrir une ligne de commande pour ce terminal : docker exec -it database bash.
Puis on se connecte à notre base de données : mysql -u user -D mymap --password="s3cr3t".
On peut alors lancer des requêtes SQL : SELECT * FROM point;

# Base de données

## 1
On exécute les commandes "docker container stop database" puis "docker container start database".

## 2

Les données qui avaient été saisies sont toujours présentes.

## 7

On exécute la commande : docker run --name database -e MYSQL_ROOT_PASSWORD=pswd -e MYSQL_DATABASE=mymap -e MYSQL_USER=user -e MYSQL_PASSWORD=s3cr3t -d --net netmap -v /home/formation/docker_course/tp1/v_hote_bdd:/var/lib/mysql mariadb:latest